package net.upsjb2.collections;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayListImpl<E> implements List {
	private Object[] store;
	private int counter;

	public ArrayListImpl() {
		this(0);
	}

	public ArrayListImpl(int size) {
		if (size < 0) {
			throw new IllegalArgumentException("Can't create = " + size);
		}
		store = new Object[size];
		counter = 0;
	}

	@Override
	public void clear() {
		counter = 0;
		this.store = new Object[counter];
	}

	@Override
	public int size() {
		return counter;
	}

	public Iterator<E> iterator() {
		return new IteratorImpl();
	}

	private class IteratorImpl implements Iterator<E> {
		private int cursor = 0;

		@Override
		public boolean hasNext() {
			return (cursor < counter);
		}

		@Override
		@SuppressWarnings("unchecked")
		public E next() {
			if (cursor == counter) {
				throw new NoSuchElementException();
			}

			cursor++;
			return (E) store[cursor - 1];
		}
	}

	@Override
	public void add(Object value) {
		if (counter == store.length) {
			Object[] copy = new Object[store.length + 1];
			System.arraycopy(store, 0, copy, 0, store.length);
			store = copy;
		}
		store[counter] = value;
		counter++;
	}

	@Override
	public void add(int index, Object value) {
		Object[] copy = new Object[store.length + 1];
		System.arraycopy(store, 0, copy, 0, index);
		System.arraycopy(store, index, copy, index + 1, counter - index);
		store = copy;
		store[index] = value;
		counter++;
	}

	@Override
	@SuppressWarnings("unchecked")
	public E set(Object value, int index) {
		if (index < 0 || index >= counter) {
			throw new IndexOutOfBoundsException();
		}
		E previousElement = (E) store[index];
		store[index] = value;
		return previousElement;
	}

	@Override
	@SuppressWarnings("unchecked")
	public E get(int index) {
		if (index < 0 || index >= counter) {
			throw new IndexOutOfBoundsException();
		}
		return (E) store[index];
	}

	@Override
	@SuppressWarnings("unchecked")
	public int indexOf(Object element) {
		for (int i = 0; i < counter; i++) {
			if (((element == null) && (store[i] == null))
					|| ((store[i] != null) && ((E) store[i]).equals((E) element))) {
				return i;
			}
		}
		return -1;
	}

	@Override
	@SuppressWarnings("unchecked")
	public E remove(int index) {
		if ((index <= counter - 1) && (index >= 0)) {
			E element = (E) store[index];
			Object[] copy = new Object[counter - 1];
			System.arraycopy(store, 0, copy, 0, index);
			System.arraycopy(store, index + 1, copy, index, counter - index - 1);
			store = copy;
			counter--;
			return element;
		} else {
			throw new NoSuchElementException("Can't find = " + index);
		}
	}

	public boolean remove(Object element) {
		int index = indexOf(element);
		if (index == -1) {
			return false;
		}
		remove(index);
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		if (counter > 0) {
			sb.append(store[0]);
		}
		for (int i = 1; i < counter; i++) {
			sb.append(',').append(' ').append(store[i]);
		}
		return sb.append(']').toString();
	}

	@Override
	public boolean isEmpty() {
		return counter == 0;
	}

	@Override
	public boolean contains(Object o) {
		return indexOf(o) >= 0;
	}

	public Object[] toArray() {
		return Arrays.copyOf(store, counter);
	}

	@SuppressWarnings("unchecked")
	public <T> T[] toArray(T[] a) {
		if (a.length < counter) {
			return (T[]) Arrays.copyOf(store, counter, a.getClass());
		}
		System.arraycopy(store, 0, a, 0, counter);
		if (a.length > counter) {
			a[counter] = null;
		}
		return a;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public int lastIndexOf(Object element) {
		int result = -1;
		for (int i = 0; i < counter; i++) {
			if (((element == null) && (store[i] == null))
					|| ((store[i] != null) && ((E) store[i]).equals((E) element))) {
				result = i;
			}
		}
		return result;
	}

}