package net.upsjb2.collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.junit.Before;
import org.junit.Test;

public class ArrayListImplTest {
	private ArrayListImpl<String> list;

	@Before
	public void setUp() {
		list = new ArrayListImpl<>();
	}

	@Test
	public void shouldAddWithArrayExtension() {
		list.add("Karol");
		list.add("Vanessa");
		list.add("Amanda");

		assertTrue(list.size() == 3);
	}

	@Test
	public void shouldReturnArraySize() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		assertTrue(list.size() == 3);
	}

	@Test
	public void shouldGetByIndex() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		assertEquals("Amanda", list.get(2));
	}

	@Test
	public void shouldRemoveByIndex() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		assertEquals("Amanda", list.remove(2));
		assertTrue(list.size() == 2);
	}

	@Test
	public void forEachTest() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		assertTrue(list.size() == 3);
	}

	@Test
	public void toStringTest() {
		list.add("Karol");
		list.add("Vanessa");
		list.add("Amanda");

		assertEquals("[Karol, Vanessa, Amanda]", list.toString());
	}

	@Test
	public void shouldSetByIndex() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		list.set("Ira", 1);

		assertEquals("Ira", list.get(1));
		assertTrue(list.size() == 3);
	}

	@Test
	public void shouldClearArray() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.clear();

		assertTrue(list.size() == 0);
	}

	@Test
	public void shouldReturnIndexOf() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		assertEquals(1, list.indexOf("Vanessa"));
	}

	@Test
	public void testListInit() {
		assertTrue(list.size() == 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testInvalidCapacity() {
		list = new ArrayListImpl<>(-1);
	}

	@Test
	public void testAddElements() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		assertEquals("Karol", list.get(0));
		assertEquals("Vanessa", list.get(1));
		assertEquals("Amanda", list.get(2));

		list.add(1, "Mariana");
		list.add(4, null);

		assertEquals("Karol", list.get(0));
		assertEquals("Mariana", list.get(1));
		assertEquals("Vanessa", list.get(2));
		assertEquals("Amanda", list.get(3));

		assertTrue(list.size() == 5);
	}

	@Test
	public void testSetElement() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");
		list.add(3, "Mila");

		list.set("Livia", 1);
		list.set(null, 3);

		assertEquals("Karol", list.get(0));
		assertEquals("Livia", list.get(1));
		assertEquals("Amanda", list.get(2));
	}

	@Test
	public void testRemoveElement() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		assertEquals(true, list.remove("Amanda"));
		assertTrue(list.size() == 2);
	}

	@Test
	public void testIterator() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		int index = 0;
		Iterator<String> iterator = list.iterator();
		while (iterator.hasNext()) {
			assertEquals(list.get(index), iterator.next());
			index++;
		}
		assertTrue(list.size() == index);
	}

	@Test(expected = NoSuchElementException.class)
	public void testRemoveWithEmptyList() {
		list.remove(0);
	}

	@Test
	public void testLastIndexOf() {
		list.add("pen");
		list.add("books");
		list.add("pencil");
		list.add("books");
		list.add("paper");

		assertEquals(3, list.lastIndexOf("books"));
	}

	@Test
	public void testArray() {
		list.add(0, "Karol");
		list.add(1, "Vanessa");
		list.add(2, "Amanda");

		assertEquals(list.toString(), Arrays.toString(list.toArray()));
	}

}